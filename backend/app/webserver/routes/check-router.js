"use strict";
const express = require("express");
const multer = require("multer");
const checkAccountSession = require("../controllers/account/check-account-session");

const router = express.Router();

module.exports = router;
